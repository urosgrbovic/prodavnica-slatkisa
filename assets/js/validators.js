function validateCategoryForm(){
    let status = true;
    const name = document.querySelector('#name').value;
    document.querySelector('#error-message').classList.add('d-none');
    document.querySelector('#error-message').innerHTML = '';
    if(!name.match(/^[A-Za-zšđžćč ]{3,}$/)){
        document.querySelector('#error-message').innerHTML += 'Ime kategorije nije ispravno! <br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }

    return status;
}
function validateOrderForm(){
    let status = true;
    const email = document.querySelector('#input_email').value;
    const forename = document.querySelector('#input_forename').value;
    const surename = document.querySelector('#input_surename').value;
    const address = document.querySelector('#input_address').value;
    const phone = document.querySelector('#input_phone').value;
    document.querySelector('#error-message').classList.add('d-none');
    document.querySelector('#error-message').innerHTML = '';
    if(!email.match(/^[a-zA-Z0-9.!#$%&’*+\/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)){
        document.querySelector('#error-message').innerHTML += 'Email nije ispravnog formata! <br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }
    if(!forename.match(/^[A-Za-z]{3,}$/)){
        document.querySelector('#error-message').innerHTML += 'Ime nije ispravnog formata! <br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }
    if(!surename.match(/^[A-Za-z- ]{3,}$/)){
        document.querySelector('#error-message').innerHTML += 'Prezime nije ispravnog formata! <br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }
    if(!address.match(/^[A-Za-z0-9-\/ ]{6,}$/)){
        document.querySelector('#error-message').innerHTML += 'Adresa nije ispravnog formata! <br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }
    if(!phone.match(/^[0-9]{6,}$/)){
        document.querySelector('#error-message').innerHTML += 'Broj telefona nije ispravnog formata! <br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }

    return status;
}
function validateIngredientForm(){
    let status = true;
    const name = document.querySelector('#name').value;
    document.querySelector('#error-message').classList.add('d-none');
    document.querySelector('#error-message').innerHTML = '';
    if(!name.match(/^[A-Za-z]{3,}$/)){
        document.querySelector('#error-message').innerHTML += 'Sastojak nije ispravnog formata! <br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }

    return status;
}
function validateProductForm(){
    let status = true;
    const name = document.querySelector('#name').value;
    const description = document.querySelector('#description').value;
    document.querySelector('#error-message').classList.add('d-none');
    document.querySelector('#error-message').innerHTML = '';
    if(!name.match(/^[A-Za-z]{3,}$/)){
        document.querySelector('#error-message').innerHTML += 'Ime nije ispravnog formata! <br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }
    if(!description.match(/^[A-Za-z0-9\/- ]{3,}$/)){
        document.querySelector('#error-message').innerHTML += 'Opis nije ispravnog formata! <br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }

    return status;
}
function validateLoginForm(){
    let status = true;
    const username = document.querySelector('#username').value;
    const password = document.querySelector('#password').value;
    document.querySelector('#error-message').classList.add('d-none');
    document.querySelector('#error-message').innerHTML = '';
    if(!username.match(/^([A-Za-z][A-Za-z0-9._-]{4,})$/)){
        document.querySelector('#error-message').innerHTML += 'Korisnicko ime nije ispravnog formata! <br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }
    if(!password.match(/^[A-Za-z0-9]{7,}$/)){
        document.querySelector('#error-message').innerHTML += 'Lozinka nije ispravnog formata! <br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }

    return status;
}
function validateRegisterForm(){
    let status = true;
    const email = document.querySelector('#email').value;
    const username = document.querySelector('#username').value;
    const password = document.querySelector('#password_1').value;
    const password1 = document.querySelector('#password_2').value;
    document.querySelector('#error-message').classList.add('d-none');
    document.querySelector('#error-message').innerHTML = '';
    if(!email.match(/^[a-zA-Z0-9.!#$%&’*+\/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)){
        document.querySelector('#error-message').innerHTML += 'Email nije ispravnog formata! <br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }
    if(!username.match(/^([A-Za-z][A-Za-z0-9._-]{4,})$/)){
        document.querySelector('#error-message').innerHTML += 'Korisnicko ime nije ispravnog formata! <br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }
    if(!password.match(/^[A-Za-z0-9]{7,}$/)){
        document.querySelector('#error-message').innerHTML += 'Lozinka nije ispravnog formata! <br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }
    if(!password1.match(/^[A-Za-z0-9]{7,}$/)){
        document.querySelector('#error-message').innerHTML += 'Ponovljena lozinka nije ispravnog formata! <br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }

    return status;
}