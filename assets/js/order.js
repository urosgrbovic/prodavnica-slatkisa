function showOrder() {
    fetch(BASE + 'api/orders', { credentials: 'include' })
        .then(result => result.json())
        .then(data => {
            displayOrder(data.orders);
        });
}

function addToOrder(productId) {
    fetch(BASE + 'api/orders/add/' + productId, { credentials: 'include' })
        .then(result => result.json())
        .then(data => {
            if (data.error === 0) {
                showOrder();
            }
        });
}

function clearOrder() {
    fetch(BASE + 'api/orders/clear', { credentials: 'include' })
        .then(result => result.json())
        .then(data => {
            if (data.error === 0) {
                showOrder();
            }
        });
}
function enableDisableButton(orders) {
    document.querySelector("#checkout").href = BASE + 'order';
    document.querySelector("#checkout").classList.remove('disabled');

    if (orders.length == 0) {
        document.querySelector('.order').innerHTML = 'Korpa je trenutno prazna!';
        document.querySelector("#checkout").href = '#';
        document.querySelector("#checkout").classList.add('disabled');
        
        return;
    }
}

function displayOrder(orders) {
    const ordersDiv = document.querySelector('.order');
    ordersDiv.innerHTML = '';
    const priceDiv = document.querySelector('.price');
    priceDiv.innerHTML = '';

    enableDisableButton(orders);

    for (order of orders) {
        const orderLink = document.createElement('a');
        orderLink.style.display = 'block';
        orderLink.innerHTML = order.name;
        orderLink.href = BASE + 'product/' + order.product_id;

        ordersDiv.appendChild(orderLink);
        const price = document.createElement('span');
        price.style.display = 'block';
        price.innerHTML = order.price;
        priceDiv.appendChild(price);
    }
}

addEventListener('load', showOrder);
