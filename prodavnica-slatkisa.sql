-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 11, 2018 at 12:04 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `prodavnica-slatkisa`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(10) UNSIGNED NOT NULL,
  `username` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `password` varchar(124) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `username`, `email`, `password`, `created_at`) VALUES
(1, 'Uros1', 'uros@gmail.com', '$2y$10$8dc99M6Lmq.DLSoiaacLi.aH0wMsGhaHTbM3vqBBmPX01R1NK9br2', '2018-09-04 22:13:09'),
(2, 'Uros1234', 'uros.grbovic.14@singimail.rs', '$2y$10$yn0uSs.LLnjiWPOjnqcRW.PwGwf1lAqyNI/ewcfHfbtzkLWW60VkW', '2018-09-10 19:55:49');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `is_visible` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `name`, `is_visible`, `created_at`) VALUES
(1, 'Cokolade', 0, '2018-09-04 22:20:10'),
(2, 'Cokoladice', 0, '2018-09-04 22:24:53'),
(3, 'Keks', 0, '2018-09-05 21:01:07'),
(4, 'Napolitanke', 0, '2018-09-05 21:01:47');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `event_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type` enum('email') NOT NULL,
  `data` text NOT NULL,
  `status` enum('pending','started','failed','done') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`event_id`, `created_at`, `type`, `data`, `status`) VALUES
(1, '2018-09-10 19:55:49', 'email', '{\"to\":[\"uros.grbovic.14@singimail.rs\"],\"cc\":[],\"bcc\":[],\"att\":[],\"subject\":\"Uspesna registracija\",\"body\":\"<!doctype html><html><head><meta charset=\\\"utf-8\\\"><body><p>Uspesno ste se registrovali: Uros1234<\\/p><p>Vase korisnicko ime je : Uros1234<\\/p><\\/body><\\/html>\",\"count\":0}', 'pending');

-- --------------------------------------------------------

--
-- Table structure for table `ingredient`
--

CREATE TABLE `ingredient` (
  `ingredient_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ingredient`
--

INSERT INTO `ingredient` (`ingredient_id`, `name`, `created_at`) VALUES
(1, 'Cokolada', '2018-09-05 22:06:11'),
(2, 'Lesnik', '2018-09-05 22:06:26'),
(3, 'Karamela', '2018-09-05 22:06:32'),
(5, 'Keks', '2018-09-08 22:30:26');

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `order_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `buyer_name` varchar(45) DEFAULT NULL,
  `buyer_surname` varchar(45) DEFAULT NULL,
  `buyer_address` varchar(45) DEFAULT NULL,
  `buyer_contact` varchar(45) DEFAULT NULL,
  `buyer_email` varchar(45) NOT NULL,
  `order_status` enum('na_cekanju','isporuceno','otkazano','') DEFAULT 'na_cekanju'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`order_id`, `created_at`, `buyer_name`, `buyer_surname`, `buyer_address`, `buyer_contact`, `buyer_email`, `order_status`) VALUES
(1, '2018-09-10 19:57:22', 'Uros', 'Grbovic', 'Danijelova 32', '065678876', 'uros.grbovic.14@singimail.rs', 'na_cekanju'),
(2, '2018-09-10 20:06:07', 'Marko', 'Markovic', 'Danijelova 32', '065654321', 'mmarkovic@gmail.com', 'isporuceno'),
(3, '2018-09-10 20:07:59', 'Pera', 'Peric', 'Danijelova 32', '061234567', 'pperic@gmail.com', 'otkazano');

-- --------------------------------------------------------

--
-- Table structure for table `order_item`
--

CREATE TABLE `order_item` (
  `order_item_id` int(10) UNSIGNED NOT NULL,
  `total_price` int(10) UNSIGNED DEFAULT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_item`
--

INSERT INTO `order_item` (`order_item_id`, `total_price`, `product_id`, `order_id`, `created_at`) VALUES
(1, 100, 4, 1, '2018-09-10 19:57:22'),
(2, 56, 2, 2, '2018-09-10 20:06:07'),
(3, 41, 3, 3, '2018-09-10 20:07:59');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `price` double UNSIGNED DEFAULT NULL,
  `originCountry` varchar(45) DEFAULT NULL,
  `colour` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` text,
  `category_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL,
  `image` varchar(120) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `price`, `originCountry`, `colour`, `name`, `description`, `category_id`, `admin_id`, `image`, `created_at`) VALUES
(1, 47.9, 'Srbija', 'Braon', 'Mars', 'Mars Cokoladica', 2, 1, '1.jpg', '2018-09-08 22:22:50'),
(2, 55.8, 'Srbija', 'Braon', 'Twix', 'Twix cokoladica', 2, 1, '2.jpg', '2018-09-08 22:23:16'),
(3, 40.87, 'Srbija', 'Braon', 'Snickers', 'Snickers cokoladica...', 2, 1, '3.jpg', '2018-09-08 22:23:48'),
(4, 100, 'Srbija', 'Braon', 'Milka', 'Milka cokolada', 1, 1, '4.jpg', '2018-09-08 22:27:17'),
(5, 70.6, 'Srbija', 'Krem', 'Plazma', 'Plazma keks', 3, 1, '5.png', '2018-09-08 22:28:14'),
(6, 80.9, 'Srbija', 'Krem', 'Mini plazma', 'mini plazma keks', 3, 1, '6.jpg', '2018-09-08 22:28:42'),
(7, 120.9, 'Srbija', 'Crna', 'Cokoladna napolitanka', 'Napolitanka sa cokoladnim prelivom', 4, 1, '7.png', '2018-09-08 22:29:35'),
(8, 90.99, 'Srbija', 'Crna', 'Nela', 'Napolitanka nela', 4, 1, '8.jpg', '2018-09-08 22:30:01');

-- --------------------------------------------------------

--
-- Table structure for table `product_ingredient`
--

CREATE TABLE `product_ingredient` (
  `product_ingredient_id` int(10) UNSIGNED NOT NULL,
  `ingredient_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_ingredient`
--

INSERT INTO `product_ingredient` (`product_ingredient_id`, `ingredient_id`, `product_id`, `created_at`) VALUES
(1, 1, 1, '2018-09-08 22:30:50'),
(2, 3, 1, '2018-09-08 22:30:56'),
(3, 1, 2, '2018-09-08 22:31:01'),
(4, 3, 2, '2018-09-08 22:31:06'),
(5, 1, 3, '2018-09-08 22:31:11'),
(6, 3, 3, '2018-09-08 22:31:16'),
(7, 1, 4, '2018-09-08 22:31:22'),
(8, 5, 5, '2018-09-08 22:31:29'),
(9, 5, 6, '2018-09-08 22:31:36'),
(10, 1, 7, '2018-09-08 22:31:42'),
(11, 1, 8, '2018-09-08 22:31:47');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`),
  ADD UNIQUE KEY `username_UNIQUE` (`username`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `ingredient`
--
ALTER TABLE `ingredient`
  ADD PRIMARY KEY (`ingredient_id`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `order_item`
--
ALTER TABLE `order_item`
  ADD PRIMARY KEY (`order_item_id`,`order_id`),
  ADD UNIQUE KEY `product_productID_UNIQUE` (`product_id`),
  ADD UNIQUE KEY `order_orderID_UNIQUE` (`order_id`),
  ADD KEY `fk_order_item_product1_idx` (`product_id`),
  ADD KEY `fk_order_item_order1_idx` (`order_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`),
  ADD UNIQUE KEY `imagre_url_UNIQUE` (`image`),
  ADD KEY `fk_product_category_idx` (`category_id`),
  ADD KEY `fk_product_admin_id` (`admin_id`);

--
-- Indexes for table `product_ingredient`
--
ALTER TABLE `product_ingredient`
  ADD PRIMARY KEY (`product_ingredient_id`),
  ADD KEY `fk_product_ingredient_ingredient1_idx` (`ingredient_id`),
  ADD KEY `fk_product_ingredient_product1_idx` (`product_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ingredient`
--
ALTER TABLE `ingredient`
  MODIFY `ingredient_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `order_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `order_item`
--
ALTER TABLE `order_item`
  MODIFY `order_item_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `product_ingredient`
--
ALTER TABLE `product_ingredient`
  MODIFY `product_ingredient_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `order_item`
--
ALTER TABLE `order_item`
  ADD CONSTRAINT `fk_order_item_order1` FOREIGN KEY (`order_id`) REFERENCES `order` (`order_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_order_item_product1` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `fk_product_category` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_ingredient`
--
ALTER TABLE `product_ingredient`
  ADD CONSTRAINT `fk_product_ingredient_ingredient1` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredient` (`ingredient_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_ingredient_product1` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
