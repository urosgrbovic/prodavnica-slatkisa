<?php
    namespace App\Controllers;

    class ProductController extends \App\Core\Controller{

        public function show($id){
            $productModel = new \App\Models\ProductModel($this->getDatabaseConnection());
            $products = $productModel->getAllByProductId($id);

            if(!$products){
                header(\Configuration::BASE);
                exit;
            }

            $this->set('products', $products);

            $productModel = new \App\Models\ProductModel($this->getDatabaseConnection());
            $productIngredients = $productModel->getAllByProductIngredient($id);
            $this->set('productIngredients', $productIngredients);
        }
        private function normaliseKeywords(string $keywords): string {
            $keywords = \trim($keywords);
            $keywords = \preg_replace('/ +/', ' ', $keywords);

            return $keywords;
        }

        public function postSearch(){
            $productModel = new \App\Models\ProductModel($this->getDatabaseConnection());

            $q = filter_input(INPUT_POST, 'q', FILTER_SANITIZE_STRING);
            $keywords = $this->normaliseKeywords($q);

            $products = $productModel->getAllBySearch($keywords);

            $this->set('products', $products);
        }
        public function postFilter(){
            $productModel = new \App\Models\ProductModel($this->getDatabaseConnection());
    
            $price = filter_input(INPUT_POST, 'price', FILTER_SANITIZE_STRING);
            $price_to = filter_input(INPUT_POST, 'price_to', FILTER_SANITIZE_STRING);

            $products = $productModel->getAllByFilter($price, $price_to);
            $this->set('products', $products);
        }
    }

    