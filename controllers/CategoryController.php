<?php
    namespace App\Controllers;

    class CategoryController extends \App\Core\Controller{

        public function show($id){
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $category = $categoryModel->getById($id);

            if(!$category){
                header(\Configuration::BASE);
                exit;
            }

            $this->set('category', $category);

            $productModel = new \App\Models\ProductModel($this->getDatabaseConnection());
            $productsInCategory = $productModel->getAllByCategoryId($id);

            $this->set('productsInCategory', $productsInCategory);

        }
    }