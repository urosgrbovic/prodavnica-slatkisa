<?php
    namespace App\Controllers;

    class IngredientController extends \App\Core\Controller{

        public function show($id){
            $ingredientModel = new \App\Models\IngredientModel($this->getDatabaseConnection());
            $ingredient = $ingredientModel->getById($id);

            if(!$ingredient){
                header(\Configuration::BASE);
                exit;
            }

            $this->set('ingredient', $ingredient);
        }
    }