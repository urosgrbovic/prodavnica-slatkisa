<?php
    namespace App\Controllers;
    use App\Core\Role\AdminRoleController;
    use App\Validators\StringValidator;
    use App\Validators\NumberValidator;
    use App\Models\ProductIngredientModel;
    

    class AdminProductIngredientManagementController extends AdminRoleController {

        public function productIngredients() {
            $productIngredientModel = new ProductIngredientModel($this->getDatabaseConnection());
            $productIngredients = $productIngredientModel->getAllByProductIngredient();

            $this->set('productIngredients', $productIngredients);
            
        }

        public function getEdit($productIngredientId){
            $productIngredientModel = new ProductIngredientModel($this->getDatabaseConnection());
            $productIngredient = $productIngredientModel->getById($productIngredientId);

            if( !$productIngredient ){
                $this->redirect(\Configuration::BASE . 'admin/productIngredients');
            }

            $this->set('productIngredient', $productIngredient);
            
            $ingredientModel = new \App\Models\IngredientModel($this->getDatabaseConnection());
            $ingredients = $ingredientModel->getAll();
            $this->set('ingredients', $ingredients);

            $productModel = new \App\Models\ProductModel($this->getDatabaseConnection());
            $products = $productModel->getAll();
            $this->set('products', $products);

            return $productIngredientModel;
        }

        public function postEdit($productIngredientId){
            $productIngredientModel = new ProductIngredientModel($this->getDatabaseConnection());
            $productIngredientModel =  $this->getEdit($productIngredientId);

            $productId = \filter_input(INPUT_POST, 'product_id', FILTER_SANITIZE_NUMBER_INT);
            $ingredientId = \filter_input(INPUT_POST, 'ingredient_id', FILTER_SANITIZE_NUMBER_INT);

            $productIngredientModel->editById($productIngredientId, [
                'product_id' => $productId,
                'ingredient_id' => $ingredientId
            ]);

            $this->redirect(\Configuration::BASE . 'admin/productIngredients');
        }

        public function getAdd(){
            $ingredientModel = new \App\Models\IngredientModel($this->getDatabaseConnection());
            $ingredients = $ingredientModel->getAll();
            $this->set('ingredients', $ingredients);

            $productModel = new \App\Models\ProductModel($this->getDatabaseConnection());
            $products = $productModel->getAll();
            $this->set('products', $products);
        }

        public function postAdd(){
            $productId = \filter_input(INPUT_POST, 'product_id', FILTER_SANITIZE_NUMBER_INT);
            $ingredientId = \filter_input(INPUT_POST, 'ingredient_id', FILTER_SANITIZE_NUMBER_INT);


            $productIngredientModel = new ProductIngredientModel($this->getDatabaseConnection());

            $productIngredientId = $productIngredientModel->add([
                'product_id' => $productId,
                'ingredient_id' => $ingredientId
            ]);

            if( $productIngredientId ){
                 $this->redirect(\Configuration::BASE . 'admin/productIngredients');
            }

            $this->set('message', 'Doslo je do greske: Nije moguce dodati sastojak u proizvod!');

            
        }
        public function delete(int $productIngredientId){
            $productIngredientModel = new ProductIngredientModel($this->getDatabaseConnection());            
            $res = $productIngredientModel->deleteById($productIngredientId);

            if( !$res){
                $this->set('message', 'Došlo je do greške: Nije obrisan sastojak iz proizvoda');
                return;
            }

            $this->redirect(\Configuration::BASE .'admin/productIngredients');
        }
    }