<?php
    namespace App\Controllers;

    use App\Validators\StringValidator;
    use App\Models\AdminModel;

    class MainController extends \App\Core\Controller{

        public function home(){
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $categories = $categoryModel->getAll();

            $this->set('categories', $categories);

            $productIngredientModel = new \App\Models\ProductIngredientModel($this->getDatabaseConnection());
            $productIngredients = $productIngredientModel->getAll();
            $this->set('productIngredients', $productIngredients);

            $ingredientModel = new \App\Models\IngredientModel($this->getDatabaseConnection());
            $ingredients = $ingredientModel->getAll();
            $this->set('ingredients', $ingredients);
        }
        public function about_us(){
            $productIngredientModel = new \App\Models\ProductIngredientModel($this->getDatabaseConnection());
            $productIngredients = $productIngredientModel->getAll();
            $this->set('productIngredients', $productIngredients);
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $categories = $categoryModel->getAll();
            $this->set('categories', $categories);
            $ingredientModel = new \App\Models\IngredientModel($this->getDatabaseConnection());
            $ingredients = $ingredientModel->getAll();
            $this->set('ingredients', $ingredients);
        }
        public function contact(){
            $productIngredientModel = new \App\Models\ProductIngredientModel($this->getDatabaseConnection());
            $productIngredients = $productIngredientModel->getAll();
            $this->set('productIngredients', $productIngredients);
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $categories = $categoryModel->getAll();
            $this->set('categories', $categories);
            $ingredientModel = new \App\Models\IngredientModel($this->getDatabaseConnection());
            $ingredients = $ingredientModel->getAll();
            $this->set('ingredients', $ingredients);
        }

        public function getRegister(){

        }
        public function postRegister(){
            $email = \filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
            $username = \filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
            $password1 = \filter_input(INPUT_POST, 'password_1', FILTER_SANITIZE_STRING);
            $password2 = \filter_input(INPUT_POST, 'password_2', FILTER_SANITIZE_STRING);

            if($password1 !== $password2){
                $this->set('message', 'Doslo je do greske: Niste uneli dva puta istu lozinku!');
                return;
            }

            $stringValidator = (new StringValidator())->setMinLength(7)->setMaxLength(120);
            if(!$stringValidator->isValid($password1)){
               $this->set('message', 'Doslo je do greske: Lozinka nije ispravnog formata!');
                return; 
            }
    
            $adminModel = new AdminModel($this->getDatabaseConnection());
            $admin = $adminModel->getByFieldName('email', $email);
            if($admin) {
                $this->set('message', 'Doslo je do greske: Vec postoji korisnik sa tom email adresom!');
                return; 
            }

            $admin = $adminModel->getByFieldName('username', $username);
            if($admin) {
                $this->set('message', 'Doslo je do greske: Vec postoji korisnik sa tim korisnickim imenom!');
                return; 
            }
            
            $stringValidator = (new StringValidator())->setMinLength(3)->setMaxLength(120);
            if(!$stringValidator->isValid($email)){
               $this->set('message', 'Doslo je do greske: Email nije ispravnog formata!');
                return; 
            }
            $stringValidator = (new StringValidator())->setMinLength(5)->setMaxLength(120);
            if(!$stringValidator->isValid($username)){
               $this->set('message', 'Doslo je do greske: Korisnicko ime nije ispravnog formata!');
                return; 
            }
            $stringValidator = (new StringValidator())->setMinLength(7)->setMaxLength(120);
            if(!$stringValidator->isValid($password2)){
               $this->set('message', 'Doslo je do greske: Ponovljena lozinka nije ispravnog formata!');
                return; 
            }

            $passwordHash = \password_hash($password1, PASSWORD_DEFAULT);

            $adminId =$adminModel->add([
                'username' => $username,
                'password' => $passwordHash,
                'email' => $email
            ]);


            if(!$adminId){
                $this->set('message', 'Doslo je do greske: Nije bilo uspesno registrovanje korisnika!');
                return;
            }

            $this->set('message', 'Uspesno ste se registrovali!');
            
            $this->notifyAdmin($adminId);

            header(\Configuration::BASE);
        }

        public function getLogin(){

        }
        public function postLogin(){
            $username = \filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
            $password = \filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);

            $stringValidator = (new StringValidator())->setMinLength(7)->setMaxLength(124);
            if(!$stringValidator->isValid($password)){
               $this->set('message', 'Doslo je do greske: Lozinka nije ispravnog formata!');
                return; 
            }
            $adminModel = new AdminModel($this->getDatabaseConnection());
            $admin = $adminModel->getByFieldName('username', $username);
            if(!$admin) {
                $this->set('message', 'Doslo je do greske: Ne postoji korisnik sa tim korisnickim imenom!');
                return; 
            }
            $stringValidator = (new StringValidator())->setMinLength(5)->setMaxLength(120);
            if(!$stringValidator->isValid($username)){
               $this->set('message', 'Doslo je do greske: Korisnicko ime nije ispravnog formata!');
                return; 
            }
            

            if(!password_verify($password, $admin->password)){
                sleep(1);
                $this->set('message', 'Doslo je do greske: Lozinka nije ispravna!');
                return;
            }



            $this->getSession()->put('admin_id', $admin->admin_id);
            $this->getSession()->save();

            $this->redirect(\Configuration::BASE . 'admin/profile');


        }
        public function logout(){
            $this->getSession()->remove('admin_id');
            $this->getSession()->save();
            $this->getSession()->regenerate();

            $this->redirect(\Configuration::BASE);
        }
        private function notifyAdmin(int $adminId){
            $adminModel = new AdminModel($this->getDatabaseConnection());
            $admin = $adminModel->getById($adminId);
            
            $admin = $adminModel->getById($admin->admin_id);
            $html = '<!doctype html><html><head><meta charset="utf-8"><body>';
            $html .= '<p>Uspesno ste se registrovali: ' . \htmlspecialchars($admin->username) . '</p>';
            $html .= '<p>Vase korisnicko ime je : ' . \htmlspecialchars($admin->username) . '</p>';
            $html .= '</body></html>';

            $event = new \App\EventHandlers\EmailEventHandler();
            $event->setSubject('Uspesna registracija');
            $event->setBody($html);
            $event->addAddress($admin->email);

            $eventModel = new \App\Models\EventModel($this->getDatabaseConnection());
            $eventModel->add([
                'type' => 'email',
                'data' => $event->getData()
            ]);

        }

    }