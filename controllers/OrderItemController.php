<?php
    namespace App\Controllers;

    class OrderItemController extends \App\Core\Role\AdminRoleController {
        public function show($id){
            $orderItemModel = new \App\Models\OrderItemModel($this->getDatabaseConnection());
            $orderItems = $orderItemModel->getAllOrderById($id);

            if(!$orderItems){
                header(\Configuration::BASE);
                exit;
            }

            $this->set('orderItems', $orderItems);

        }
    }