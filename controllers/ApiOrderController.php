<?php
    namespace App\Controllers;

    class ApiOrderController extends \App\Core\ApiController {
        public function showOrder() {
            $orders = $this->getSession()->get('orders', []);
            $this->set('orders', $orders);
        }

        public function addToOrder($productId) {
            $productModel = new \App\Models\ProductModel($this->getDatabaseConnection());
            $product = $productModel->getById($productId);

            if (!$product) {
                $this->set('error', -1);
                return;
            }

            $orders = $this->getSession()->get('orders', []);

            foreach ($orders as $order) {
                if ($order->product_id == $productId) {
                    $this->set('error', -2);
                    return;
                }
            }

            $orders[] = $product;
            $this->getSession()->put('orders', $orders);

            $this->set('error', 0);
        }

        public function clear() {
            $this->getSession()->put('orders', []);

            $this->set('error', 0);
        }
    }
