<?php
    namespace App\Controllers;

    class ProductIngredientController extends \App\Core\Controller{

        public function show($id){
            $productIngredientModel = new \App\Models\ProductIngredientModel($this->getDatabaseConnection());
            $productIngredient = $ProductIngredientModel->getById($id);

            if(!$productIngredient){
                header(\Configuration::BASE);
                exit;
            }

            $this->set('productIngredient', $productIngredient);
        }
    }