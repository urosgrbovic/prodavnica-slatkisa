<?php
    namespace App\Controllers;
    use App\Core\Role\AdminRoleController;
    use App\Validators\StringValidator;
    use App\Validators\NumberValidator;
    use App\Models\ProductModel;

    class AdminProductManagementController extends AdminRoleController {
        public function products() {
            $productModel = new \App\Models\ProductModel($this->getDatabaseConnection());
            $products = $productModel->getAll();

            $this->set('products', $products);
        }

        public function getEdit($productId){
            $productModel = new \App\Models\ProductModel($this->getDatabaseConnection());
            $product = $productModel->getById($productId);

            if( !$product ){
                $this->redirect(\Configuration::BASE . 'admin/products');
            }

            $this->set('product', $product);

            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $categories = $categoryModel->getAll();

            $this->set('categories', $categories);

            return $productModel;
        }

        public function postEdit($productId){
            $productModel =  $this->getEdit($productId);

            $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
            $description = filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);
            $price = filter_input(INPUT_POST, 'price', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
            $categoryId = filter_input(INPUT_POST, 'category_id', FILTER_SANITIZE_NUMBER_INT);
            $colour = filter_input(INPUT_POST, 'colour', FILTER_SANITIZE_STRING);
            $originCountry = filter_input(INPUT_POST, 'originCountry', FILTER_SANITIZE_STRING);
            $quantity = filter_input(INPUT_POST, 'quantity', FILTER_SANITIZE_STRING);

            $stringValidator = (new StringValidator())->setMinLength(3)->setMaxLength(120);
            if(!$stringValidator->isValid($name)){
               $this->set('message', 'Doslo je do greske: Ime  proizvoda nije ispravnog formata!');
                return; 
            }
            $stringValidator = (new StringValidator())->setMaxLength(6*1024);
            if(!$stringValidator->isValid($description)){
               $this->set('message', 'Doslo je do greske: Opis nije ispravnog formata!');
                return; 
            }
            $numberValidator = (new NumberValidator())->setDecimal()->setUnsigned()->setIntegerLength(7)->setMaxDecimalDigits(2);
            if(!$stringValidator->isValid($price)){
               $this->set('message', 'Doslo je do greske: Cena nije ispravnog formata!');
                return; 
            }
            $stringValidator = (new StringValidator())->setMinLength(3)->setMaxLength(120);
            if(!$stringValidator->isValid($colour)){
               $this->set('message', 'Doslo je do greske: colour dela nije ispravnog formata!');
                return; 
            }
            $stringValidator = (new StringValidator())->setMinLength(3)->setMaxLength(120);
            if(!$stringValidator->isValid($originCountry)){
               $this->set('message', 'Doslo je do greske: originCountry nije ispravnog formata!');
                return; 
            }


            $productModel = new ProductModel($this->getDatabaseConnection());
            $product = $productModel->getByFieldName('name', $name);
            if($product) {
                $this->set('message', 'Doslo je do greske: Vec postoji proizvod sa tim imenom!');
                return; 
            }

            $res = $productModel->editById($productId, [
                'name' => $name,
                'description' => $description,
                'price' => $price,
                'category_id' => $categoryId,
                'colour' => $colour,
                'admin_id' => $this->getSession()->get('admin_id'),
                'originCountry' => $originCountry,
                'quantity' => $quantity
            ]);

            if( !$res ){
                $this->set('message', 'Doslo je do greske: Nije moguce izmeniti proizvod!');
            }

            if($_FILES['image'] && $_FILES['image']['error'] == 0){
                $uploadStatus = $this->doImageUpload('image', $productId);
                if(!$uploadStatus) {
                    return;
                }
            }

            $this->redirect(\Configuration::BASE . 'admin/products');
        }

        public function getAdd(){
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $categories = $categoryModel->getAll();

            $this->set('categories', $categories);
        }

        public function postAdd(){
            $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
            $description = filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);
            $price = filter_input(INPUT_POST, 'price', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
            $categoryId = filter_input(INPUT_POST, 'category_id', FILTER_SANITIZE_NUMBER_INT);
            $colour = filter_input(INPUT_POST, 'colour', FILTER_SANITIZE_STRING);
            $originCountry = filter_input(INPUT_POST, 'originCountry', FILTER_SANITIZE_STRING);
            $quantity = filter_input(INPUT_POST, 'quantity', FILTER_SANITIZE_STRING);

            $stringValidator = (new StringValidator())->setMinLength(3)->setMaxLength(120);
            if(!$stringValidator->isValid($name)){
               $this->set('message', 'Doslo je do greske: Ime  proizvoda nije ispravnog formata!');
                return; 
            }
            $stringValidator = (new StringValidator())->setMaxLength(6*1024);
            if(!$stringValidator->isValid($description)){
               $this->set('message', 'Doslo je do greske: Opis nije ispravnog formata!');
                return; 
            }
            $numberValidator = (new NumberValidator())->setDecimal()->setUnsigned()->setIntegerLength(7)->setMaxDecimalDigits(2);
            if(!$stringValidator->isValid($price)){
               $this->set('message', 'Doslo je do greske: Cena nije ispravnog formata!');
                return; 
            }
            $stringValidator = (new StringValidator())->setMinLength(3)->setMaxLength(120);
            if(!$stringValidator->isValid($colour)){
               $this->set('message', 'Doslo je do greske: colour dela nije ispravnog formata!');
                return; 
            }
            $stringValidator = (new StringValidator())->setMinLength(3)->setMaxLength(120);
            if(!$stringValidator->isValid($originCountry)){
               $this->set('message', 'Doslo je do greske: originCountry nije ispravnog formata!');
                return; 
            }

            $productModel = new ProductModel($this->getDatabaseConnection());
            $product = $productModel->getByFieldName('name', $name);
            if($product) {
                $this->set('message', 'Doslo je do greske: Vec postoji proizvod sa tim imenom!');
                return; 
            }

            $productId = $productModel->add([
                'name' => $name,
                'description' => $description,
                'price' => $price,
                'category_id' => $categoryId,
                'colour' => $colour,
                'admin_id' => $this->getSession()->get('admin_id'),
                'originCountry' => $originCountry,
                'quantity' => $quantity
            ]);
        

            if( !$productId ){
                $this->set('message', 'Doslo je do greske: Nije moguce dodati proizvod!');
            }
            
            $uploadStatus = $this->doImageUpload('image', $productId);
            if(!$uploadStatus) {
                return;
            }

            $this->redirect(\Configuration::BASE . 'admin/products');
            
        }
        private function doImageUpload(string $fieldName, string $productId){
            $productModel = new ProductModel($this->getDatabaseConnection());
            $product = $productModel->getById(intval($productId));

            unlink(\Configuration::UPLOAD_DIR . $product->image);

            $uploadPath = new \Upload\Storage\FileSystem(\Configuration::UPLOAD_DIR);
            $file = new \Upload\File($fieldName, $uploadPath);
            $file->setName($productId);
            $file->addValidations([
                new \Upload\Validation\Mimetype(["image/jpeg", "image/png"]),
                new \Upload\Validation\Size("5M")
            ]);

            try{
                $file->upload();

                $fullName = $file->getNameWithExtension();

                $stringValidator = (new StringValidator())->setMaxLength(120);
                if(!$stringValidator->isValid($fullName)){
                $this->set('message', 'Doslo je do greske: Putanja nije ispravnog formata!');
                    return; 
                }

                $productModel->editById(intval($productId), [
                    'image' => $fullName
                ]);
                
                $this->doResize(
                    \Configuration::UPLOAD_DIR . $fullName,
                    \Configuration::DEFAULT_IMAGE_WIDTH,
                    \Configuration::DEFAULT_IMAGE_HEIGHT
                );

                return true;
            } catch (Exception $e) {
                $this->set('message', 'Greska' . \implode(', ', $file.getErrors()) );
                return false;
            }
        }
        private function doResize(string $filePath, int $w, int $h){
            $longer = max($w, $h);

            $image = new \Gumlet\ImageResize($filePath);
            $image->resizeToBestFit($longer, $longer);
            $image->crop($w, $h);
            $image->save($filePath);
        }
        public function delete(int $productId){
            $productModel = new ProductModel($this->getDatabaseConnection());            
            $res = $productModel->deleteById($productId);

            if( !$res){
                $this->set('message', 'Došlo je do greške: Nije obrisan sastojak');
                return;
            }

            $this->redirect(\Configuration::BASE .'admin/products');
        }
    }