<?php
    namespace App\Controllers;
    use App\Validators\StringValidator;
    use App\Validators\NumberValidator;
    use App\Models\OrderModel;

    class AdminOrderManagementController extends \App\Core\Controller {
        public function getOrder(){

        }
        public function postOrder(){
            $buyerName = \filter_input(INPUT_POST, 'buyer_name', FILTER_SANITIZE_STRING);
            $buyerSurname = \filter_input(INPUT_POST, 'buyer_surname', FILTER_SANITIZE_STRING);
            $buyerAddress = \filter_input(INPUT_POST, 'buyer_address', FILTER_SANITIZE_STRING);
            $buyerContact = \filter_input(INPUT_POST, 'buyer_contact', FILTER_SANITIZE_STRING);
            $buyerEmail = \filter_input(INPUT_POST, 'buyer_email', FILTER_SANITIZE_EMAIL);

            $orderModel = new OrderModel($this->getDatabaseConnection());

            $stringValidator = (new StringValidator())->setMinLength(3)->setMaxLength(45);
            if(!$stringValidator->isValid($buyerName)){
               $this->set('message', 'Doslo je do greske: Ime nije ispravnog formata!');
                return; 
            }
            $stringValidator = (new StringValidator())->setMinLength(3)->setMaxLength(45);
            if(!$stringValidator->isValid($buyerSurname)){
               $this->set('message', 'Doslo je do greske: Prezime nije ispravnog formata!');
                return; 
            }
            $stringValidator = (new StringValidator())->setMinLength(3)->setMaxLength(45);
            if(!$stringValidator->isValid($buyerAddress)){
               $this->set('message', 'Doslo je do greske: Adresa nije ispravnog formata!');
                return; 
            }
            $stringValidator = (new StringValidator())->setMinLength(3)->setMaxLength(45);
            if(!$stringValidator->isValid($buyerContact)){
               $this->set('message', 'Doslo je do greske: Telefon nije ispravnog formata!');
                return; 
            }

            $stringValidator = (new StringValidator())->setMinLength(3)->setMaxLength(45);
            if(!$stringValidator->isValid($buyerEmail)){
               $this->set('message', 'Doslo je do greske: Email nije ispravnog formata!');
                return; 
            }

            $products = $this->getSession()->get('orders', []);

            $orderId = $orderModel->add([
                'buyer_name' => $buyerName,
                'buyer_surname' => $buyerSurname,
                'buyer_address' => $buyerAddress,
                'buyer_contact' => $buyerContact,
                'buyer_email' => $buyerEmail,
                'order_status' => 'na_cekanju'
            ]);

            if (!$orderId){
                $this->set('message', 'Doslo je do greske: Nije bilo uspesno dodavanje u korpu!');
                return;
            }

            $orderItemModel = new \App\Models\OrderItemModel($this->getDatabaseConnection());

            foreach ($products as $product) {
                $orderItemId = $orderItemModel->add([
                    'order_id' => $orderId,
                    'product_id' => $product->product_id,
                    'total_price' => $product->price
                ]);
            }

            $products = $this->getSession()->put('orders', []);
            $msg = "Uspesno ste dodali u korpu! Vaš broj porudzbine je: " . $orderId;
            $this->set('message', $msg);
        }
        
    }