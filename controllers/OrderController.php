<?php
    namespace App\Controllers;

    class OrderController extends \App\Core\Role\AdminRoleController {
        public function home(){
            $orderModel = new \App\Models\OrderModel($this->getDatabaseConnection());
            $orders = $orderModel->getAll();

            $this->set('orders', $orders);

        }
        
        public function delete(int $orderId){
            $orderModel = new \App\Models\OrderModel($this->getDatabaseConnection());            
            $res = $orderModel->deleteById($orderId);

            if( !$res){
                $this->set('message', 'Došlo je do greške: Nije obrisan artikal iz korpe');
                return;
            }

            $this->redirect(\Configuration::BASE .'admin/orders');
        }

        public function getEdit($orderId){
            $orderModel = new \App\Models\OrderModel($this->getDatabaseConnection());
            $order = $orderModel->getById($orderId);
            if( !$order ){
                $this->redirect(\Configuration::BASE . 'admin/orders');
            }
            $this->set('order', $order);

            return $orderModel;
        }

        public function postEdit($orderId){
            $orderModel = new \App\Models\OrderModel($this->getDatabaseConnection());

            $orderStatus = filter_input(INPUT_POST, 'order_status', FILTER_SANITIZE_STRING);

            $orderModel->editById($orderId, [
                'order_status' => $orderStatus
            ]);

            $this->redirect(\Configuration::BASE . 'admin/orders');
        }
    }