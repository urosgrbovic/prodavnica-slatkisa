<?php
    namespace App\Controllers;
    use App\Core\Role\AdminRoleController;
    use App\Validators\StringValidator;
    use App\Models\IngredientModel;

    class AdminIngredientManagementController extends AdminRoleController {

        public function ingredients() {
            $ingredientModel = new IngredientModel($this->getDatabaseConnection());
            $ingredients = $ingredientModel->getAll();

            $this->set('ingredients', $ingredients);
        }

        public function getEdit($ingredientId){
            $ingredientModel = new IngredientModel($this->getDatabaseConnection());
            $ingredient = $ingredientModel->getById($ingredientId);

            if( !$ingredient ){
                $this->redirect(\Configuration::BASE . 'admin/ingredients');
            }

            $this->set('ingredient', $ingredient);

            return $ingredientModel;
        }

        public function postEdit($ingredientId){
            $ingredientModel =  $this->getEdit($ingredientId);

            $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);

            $stringValidator = (new StringValidator())->setMinLength(3)->setMaxLength(120);
            if(!$stringValidator->isValid($name)){
               $this->set('message', 'Doslo je do greske: Ime sastojak nije ispravnog formata!');
                return; 
            }

            $ingredientModel->editById($ingredientId, [
                'name' => $name
            ]);

            $this->redirect(\Configuration::BASE . 'admin/ingredients');
        }

        public function getAdd(){

        }

        public function postAdd(){
            $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);

            $stringValidator = (new StringValidator())->setMinLength(3)->setMaxLength(120);
            if(!$stringValidator->isValid($name)){
               $this->set('message', 'Doslo je do greske: Ime sastojak nije ispravnog formata!');
                return; 
            }

            $ingredientModel = new IngredientModel($this->getDatabaseConnection());          
            $ingredient = $ingredientModel->getByFieldName('name', $name);
            if($ingredient) {
                $this->set('message', 'Doslo je do greske: Vec postoji sastojak sa tim imenom!');
                return; 
            }
            $ingredientId = $ingredientModel->add([
                'name' => $name
            ]);

            if( $ingredientId ){
                 $this->redirect(\Configuration::BASE . 'admin/ingredients');
            }

            $this->set('message', 'Doslo je do greske: Nije moguce dodati sastojak!');
        }
        public function delete(int $ingredientId){
            $ingredientModel = new IngredientModel($this->getDatabaseConnection());            
            $res = $ingredientModel->deleteById($ingredientId);

            if( !$res){
                $this->set('message', 'Došlo je do greške: Nije obrisan sastojak');
                return;
            }

            $this->redirect(\Configuration::BASE .'admin/ingredients');
        }
    }