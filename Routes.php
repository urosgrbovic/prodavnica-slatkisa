<?php
    return [
        App\Core\Route::get('|^admin/register/?$|', 'Main', 'getRegister'),
        App\Core\Route::post('|^admin/register/?$|', 'Main', 'postRegister'),
        App\Core\Route::get('|^admin/login/?$|', 'Main', 'getLogin'),
        App\Core\Route::post('|^admin/login/?$|', 'Main', 'postLogin'),
        App\Core\Route::get('|^admin/logout/?$|', 'Main', 'logout'),

        App\Core\Route::get('|^about-us/?$|', 'Main', 'about_us'),
        App\Core\Route::get('|^contact/?$|', 'Main', 'contact'),

        App\Core\Route::get('|^category/([0-9]+)/?$|', 'Category', 'show'),

        App\Core\Route::get('|^product/([0-9]+)/?$|', 'Product', 'show'),
        App\Core\Route::get('|^product_ingredient/([0-9]+)/?$|', 'ProductIngredient', 'show'),
        App\Core\Route::get('|^ingredient/([0-9]+)/?$|', 'Ingredient', 'show'),
        App\Core\Route::post('|^search/?$|', 'Product', 'postSearch'),
        App\Core\Route::post('|^filter/?$|', 'Product', 'postFilter'),
        App\Core\Route::get('|^handle/([a-z]+)/?$|', 'EventHandler', 'handle'),

        #Api rute:
        App\Core\Route::get('|^api/orders/?$|', 'ApiOrder', 'showOrder'),
        App\Core\Route::get('|^api/orders/add/([0-9]+)/?$|', 'ApiOrder', 'addToOrder'),
        App\Core\Route::get('|^api/orders/clear/?$|', 'ApiOrder', 'clear'),

        #Admin role routes:
        App\Core\Route::get('|^admin/profile/?$|', 'AdminDashboard', 'index'),
        #category table:
        App\Core\Route::get('|^admin/categories/?$|', 'AdminCategoryManagement', 'categories'),
        App\Core\Route::get('|^admin/categories/edit/([0-9]+)/?$|', 'AdminCategoryManagement', 'getEdit'),
        App\Core\Route::post('|^admin/categories/edit/([0-9]+)/?$|', 'AdminCategoryManagement', 'postEdit'),
        App\Core\Route::get('|^admin/categories/add/?$|', 'AdminCategoryManagement', 'getAdd'),
        App\Core\Route::post('|^admin/categories/add/?$|', 'AdminCategoryManagement', 'postAdd'),
        App\Core\Route::get('|^admin/categories/delete/([0-9]+)/?$|', 'AdminCategoryManagement', 'delete'),
        #product table:
        App\Core\Route::any('|^admin/products/?$|', 'AdminProductManagement', 'products'),
        App\Core\Route::get('|^admin/products/edit/([0-9]+)/?$|', 'AdminProductManagement', 'getEdit'),
        App\Core\Route::post('|^admin/products/edit/([0-9]+)/?$|', 'AdminProductManagement', 'postEdit'),
        App\Core\Route::get('|^admin/products/add/?$|', 'AdminProductManagement', 'getAdd'),
        App\Core\Route::post('|^admin/products/add/?$|', 'AdminProductManagement', 'postAdd'),
        App\Core\Route::get('|^admin/products/delete/([0-9]+)/?$|', 'AdminProductManagement', 'delete'),
        #ingredient table:
        App\Core\Route::get('|^admin/ingredients/?$|', 'AdminIngredientManagement', 'ingredients'),
        App\Core\Route::get('|^admin/ingredients/edit/([0-9]+)/?$|', 'AdminIngredientManagement', 'getEdit'),
        App\Core\Route::post('|^admin/ingredients/edit/([0-9]+)/?$|', 'AdminIngredientManagement', 'postEdit'),
        App\Core\Route::get('|^admin/ingredients/add/?$|', 'AdminIngredientManagement', 'getAdd'),
        App\Core\Route::post('|^admin/ingredients/add/?$|', 'AdminIngredientManagement', 'postAdd'),
        App\Core\Route::get('|^admin/ingredients/delete/([0-9]+)/?$|', 'AdminIngredientManagement', 'delete'),
        #product_ingredient table:     
        App\Core\Route::get('|^admin/productIngredients/?$|', 'AdminProductIngredientManagement', 'productIngredients'),
        App\Core\Route::get('|^admin/productIngredients/edit/([0-9]+)/?$|', 'AdminProductIngredientManagement', 'getEdit'),
        App\Core\Route::post('|^admin/productIngredients/edit/([0-9]+)/?$|', 'AdminProductIngredientManagement', 'postEdit'),
        App\Core\Route::get('|^admin/productIngredients/add/?$|', 'AdminProductIngredientManagement', 'getAdd'),
        App\Core\Route::post('|^admin/productIngredients/add/?$|', 'AdminProductIngredientManagement', 'postAdd'),
        App\Core\Route::get('|^admin/productIngredients/delete/([0-9]+)/?$|', 'AdminProductIngredientManagement', 'delete'),
        #order table:
        App\Core\Route::get('|^admin/order/([0-9]+)/?$|', 'OrderItem', 'show'),
        App\Core\Route::get('|^admin/orders/?$|', 'Order', 'home'),
        App\Core\Route::get('|^admin/order/delete/([0-9]+)/?$|', 'Order', 'delete'),
        App\Core\Route::get('|^admin/order/edit/([0-9]+)/?$|', 'Order', 'getEdit'),
        App\Core\Route::post('|^admin/order/edit/([0-9]+)/?$|', 'Order', 'postEdit'),
        App\Core\Route::get('|^order/?$|', 'AdminOrderManagement', 'getOrder'),
        App\Core\Route::post('|^order/?$|', 'AdminOrderManagement', 'postOrder'),
        
        App\Core\Route::any('|^.*$|', 'Main', 'home')
    ];