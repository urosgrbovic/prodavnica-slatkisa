<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;

    class IngredientModel extends Model{

        protected function getFields(): array{
            return [
                'ingredient_id' => new Field( (new NumberValidator())->setIntegerLength(10), false ),
                'created_at' => new Field( (new DateTimeValidator())->allowDate()->allowTime() , false ),

                'name' => new Field( (new StringValidator())->setMinLength(3)->setMaxLength(45) )
            ];
        }
        
    }