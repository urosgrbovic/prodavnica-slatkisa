<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;

    class ProductIngredientModel extends Model{

        protected function getFields(): array{
            return [
                'product_ingredient_id' => new Field( (new NumberValidator())->setIntegerLength(10), false ),
                'created_at' => new Field( (new DateTimeValidator())->allowDate()->allowTime() , false ),

                'product_id' => new Field( (new NumberValidator())->setIntegerLength(10) ),
                'ingredient_id' => new Field( (new NumberValidator())->setIntegerLength(10) )
                

            ];
        }

        public function getAllByProductIngredient() {
            $sql = 'SELECT product_ingredient_id, ingredient.name AS "sastojak", product.name AS "proizvod" FROM`ingredient` 
                    INNER JOIN `product_ingredient` ON ingredient.ingredient_id = product_ingredient.ingredient_id 
                    INNER JOIN `product` ON product_ingredient.product_id = product.product_id ;';     
            $prep = $this->getConnection()->prepare($sql);

            if(!$prep){
                return [];
            }

            $res = $prep->execute();
            if(!$res){
                return [];
            }

            return $prep->fetchAll(\PDO::FETCH_OBJ);
        }
        
    }