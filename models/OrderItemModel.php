<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\StringValidator;
    use App\Validators\DateTimeValidator;

    class OrderItemModel extends Model{

        protected function getFields(): array{
            return [
                'order_item_id' => new Field( (new NumberValidator())->setIntegerLength(10), false ),
                'created_at' => new Field( (new DateTimeValidator())->allowDate()->allowTime() , false ),
                'quantity' => new Field( (new NumberValidator())->setIntegerLength(10) ),
                'total_price' => new Field( (new StringValidator())->setMinLength(3)->setMaxLength(120) ),
                'product_id' => new Field( (new NumberValidator())->setIntegerLength(10) ),
                'order_id' => new Field( (new NumberValidator())->setIntegerLength(10) ),
                
                

            ];
        }  
        public function getAllOrderById($orderId) {
            $sql = 'SELECT * FROM `order_item` INNER JOIN `product` ON `order_item`.`product_id` = `product`.`product_id` WHERE `order_id` = ?;';     
            $prep = $this->getConnection()->prepare($sql);

            if(!$prep){
                return [];
            }

            $res = $prep->execute([$orderId]);
            if(!$res){
                return [];
            }

            return $prep->fetchAll(\PDO::FETCH_OBJ);
        } 

 
    }