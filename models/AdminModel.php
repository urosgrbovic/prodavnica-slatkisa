<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;

    class AdminModel extends Model{

        protected function getFields(): array{
            return [
                'admin_id' => new Field( (new NumberValidator())->setIntegerLength(10), false ),
                'created_at' => new Field( (new DateTimeValidator())->allowDate()->allowTime() , false ),
                'email' => new Field( (new StringValidator())->setMinLength(3)->setMaxLength(45) ),
                'username' => new Field( (new StringValidator())->setMinLength(3)->setMaxLength(45) ),
                'password' => new Field( (new StringValidator())->setMinLength(7)->setMaxLength(124) ),

                

            ];
        }
        
        public function getAllByCategoryId(int $categoryId): array {
            return $this->getAllByFieldName('category_id', $categoryId);
            
        }

        public function getByName(string $name){
           return $this->getByFieldName('name', $name);
        }
    }