<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;

    class OrderModel extends Model{

        protected function getFields(): array{
            return [
                'order_id' => new Field( (new NumberValidator())->setIntegerLength(10), false ),
                'created_at' => new Field( (new DateTimeValidator())->allowDate()->allowTime() , false ),

                'buyer_name' => new Field( (new StringValidator())->setMinLength(3)->setMaxLength(120) ),
                'buyer_surname' => new Field( (new StringValidator())->setMinLength(3)->setMaxLength(45) ),
                'buyer_address' => new Field( (new StringValidator())->setMinLength(3)->setMaxLength(45) ),
                'buyer_contact' => new Field( (new StringValidator())->setMinLength(3)->setMaxLength(45) ),
                'buyer_email' => new Field( (new StringValidator())->setMinLength(3)->setMaxLength(45) ),
                'order_status' => new Field( (new StringValidator())->setMinLength(3)->setMaxLength(45) ),
            ];
        }

        public function getAllOrder() {
            $sql = 'SELECT * FROM `order`;';     
            $prep = $this->getConnection()->prepare($sql);

            if(!$prep){
                return [];
            }

            $res = $prep->execute();
            if(!$res){
                return [];
            }

            return $prep->fetchAll(\PDO::FETCH_OBJ);
        }
        
    }