<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;
    use App\Validators\BitValidator;
    use App\Validators\WarrantyValidator;

    class ProductModel extends Model{

        protected function getFields(): array{
            return [
                'product_id' => new Field( (new NumberValidator())->setIntegerLength(10), false ),
                'created_at' => new Field( (new DateTimeValidator())->allowDate()->allowTime() , false ),

                'name' => new Field( (new StringValidator())->setMaxLength(45) ),
                'description' => new Field( (new StringValidator())->setMaxLength(6*1024) ),
                'price' => new Field( (new NumberValidator())->setDecimal()->setUnsigned()->setIntegerLength(7)->setMaxDecimalDigits(2) ),
                'category_id' => new Field( (new NumberValidator())->setIntegerLength(10) ),
                'originCountry' => new Field( (new StringValidator())->setMaxLength(45) ),
                'colour' => new Field( (new StringValidator())->setMaxLength(45) ),
                'quantity' => new Field( (new StringValidator())->setMaxLength(45) ),
                'image' => new Field( (new StringValidator())->setMaxLength(120) ),
                'admin_id' => new Field( (new NumberValidator())->setIntegerLength(10) ),
                

            ];
        }
        
        public function getAllByCategoryId(int $categoryId): array {
            return $this->getAllByFieldName('category_id', $categoryId);
            
        }

        public function getByName(string $name){
           return $this->getByFieldName('name', $name);
        }

        public function getAllBySearch(string $keywords) {
            $sql = 'SELECT * FROM `product` WHERE `name` LIKE ? OR `description` LIKE ?;';
            $keywords = '%' . $keywords . '%';

            $prep = $this->getConnection()->prepare($sql);

            if(!$prep){
                return [];
            }

            $res = $prep->execute([$keywords, $keywords]);
            if(!$res){
                return [];
            }

            return $prep->fetchAll(\PDO::FETCH_OBJ);
        }

        public function getAllByFilter(int $price, int $price_to) {
            $sql = 'SELECT * FROM`product` WHERE product.price BETWEEN ? AND ? ORDER BY product.price ASC;';     
            $prep = $this->getConnection()->prepare($sql);

            if(!$prep){
                return [];
            }

            $res = $prep->execute([$price, $price_to]);
            if(!$res){
                return [];
            }

            return $prep->fetchAll(\PDO::FETCH_OBJ);
        }
        public function getAllByProductId(int $productId) {
            $sql = 'SELECT * FROM `product` WHERE product_id = ?;';     
            $prep = $this->getConnection()->prepare($sql);

            if(!$prep){
                return [];
            }

            $res = $prep->execute([$productId]);
            if(!$res){
                return [];
            }

            return $prep->fetchAll(\PDO::FETCH_OBJ);
        }

        public function getAllByProductIngredient(int $productId) {
            $sql = 'SELECT price, originCountry, product.name AS "ime_proizvoda", description, image, product_ingredient.ingredient_id, product_ingredient.product_id , ingredient.name AS "sastojak" FROM`ingredient` 
            INNER JOIN `product_ingredient` ON ingredient.ingredient_id = product_ingredient.ingredient_id 
            INNER JOIN `product` ON product_ingredient.product_id = product.product_id WHERE product.product_id = ?;';     
            $prep = $this->getConnection()->prepare($sql);

            if(!$prep){
                return [];
            }

            $res = $prep->execute([$productId]);
            if(!$res){
                return [];
            }

            return $prep->fetchAll(\PDO::FETCH_OBJ);
        }

    }